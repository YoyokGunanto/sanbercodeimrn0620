// Jawaban Soal No. 1 (Range)
console.log('========== Jawaban Soal No. 1 (Range) =============');
function range(startNum = '', finishNum = '') {
    var nilai_akhir = [];
    if (startNum == '' || finishNum == '') {
        nilai_akhir = -1;
    } else {
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i++) {
                nilai_akhir.push(i);
            }
        } else if (startNum > finishNum) {
            for (var i = startNum; i >= finishNum; i--) {
                nilai_akhir.push(i);
            }
        }
    }
    return nilai_akhir;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());


// Jawaban Soal No. 2 (Range with Step)
console.log('\n\n========== Jawaban Soal No. 2 (Range with Step) =============');
function rangeWithStep(startNum = '', finishNum = '', step = '') {
    var nilai_akhir = [];
    if (startNum == '' || finishNum == '' || step == '') {
        nilai_akhir = -1;
    } else {
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i += step) {
                nilai_akhir.push(i);
            }
        } else if (startNum > finishNum) {
            for (var i = startNum; i >= finishNum; i -= step) {
                nilai_akhir.push(i);
            }
        }
    }
    return nilai_akhir;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));


// Jawaban Soal No. 3 (Sum of Range)
console.log('\n\n========== Jawaban Soal No. 3 (Sum of Range) =============');
function sum(startNum = '', finishNum = '', step = 1) {
    var nilai_akhir = [], hasil_sum = 0;
    if (startNum == '' && finishNum == '') {
        hasil_sum = 0;
    } else if (startNum == '') {
        hasil_sum = 1;
    } else {
        if (startNum <= finishNum) {
            for (var i = startNum; i <= finishNum; i += step) {
                nilai_akhir.push(i);
                hasil_sum = nilai_akhir.reduce(function (acc, val) { return acc + val; }, 0);
            }
        } else if (startNum > finishNum) {
            for (var i = startNum; i >= finishNum; i -= step) {
                nilai_akhir.push(i);
                hasil_sum = nilai_akhir.reduce(function (acc, val) { return acc + val; }, 0);
            }
        }
    }
    return hasil_sum;
}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());


// Jawaban Soal No. 4 (Array Multidimensi)
console.log('\n\n========== Jawaban Soal No. 4 (Array Multidimensi) =============');
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
for (var i = 0; i < input.length; i++) {
    console.log('Nomor ID\t: ' + input[i][0]);
    console.log('Nama\t\t: ' + input[i][1]);
    console.log('TTL\t\t: ' + input[i][2] + ', ' + input[i][3]);
    console.log('Hobi\t\t: ' + input[i][4]);
    console.log('\n');
}


// Jawaban Soal No. 5 (Balik Kata)
console.log('\n\n========== Jawaban Soal Soal No. 5 (Balik Kata) =============');
function balikKata(kata = '') {
    var hasil = '';
    for (var i = kata.length - 1; i >= 0; i--) {
        hasil = hasil + kata[i];
    }
    return hasil;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));


// Jawaban Soal No. 6 (Metode Array)
console.log('\n\n========== Jawaban Soal No. 6 (Metode Array) =============');
function dataHandling2(kata2 = ['', '', '', '', '']) {
    var arr = [];
    arr[0] = kata2[0];
    arr[1] = kata2[1] + ' Elsharawy';
    arr[2] = 'Provinsi ' + kata2[2];
    arr[3] = kata2[3];
    arr[4] = kata2[4];
    arr.splice(4, 1, 'Pria', 'SMA Internasional Metro');
    // Jawaban 1
    console.log(arr);
    var arr_split = arr[3].split('/'), bulan_nama = '';
    switch (arr_split[1]) {
        case '01': { bulan_nama = 'Januari'; break; }
        case '02': { bulan_nama = 'Februari'; break; }
        case '03': { bulan_nama = 'Maret'; break; }
        case '04': { bulan_nama = 'April'; break; }
        case '05': { bulan_nama = 'Mei'; break; }
        case '06': { bulan_nama = 'Juni'; break; }
        // Dan seterusnya
    }
    // Jawaban 2
    console.log(bulan_nama);
    var arr_join = arr_split.join("-");
    var arr_sort = arr_split.sort(function (a, b) { return b - a });
    // arr.map(Number); // untuk convert ke array string ke number
    // Jawaban 3
    console.log(arr_sort);
    // Jawaban 4
    console.log(arr_join);
    var arr_slice = String(arr[1]).slice(0, 15);
    // Jawaban 5
    console.log(arr_slice);
}
var kata_array = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(kata_array);