// Jawaban Soal No. 1
console.log('\n======== Jawaban Soal No. 1 ========');
function teriak() {
    return 'Halo Sanbers!';
}
console.log(teriak());


// Jawaban Soal No. 2
console.log('\n\n======== Jawaban Soal No. 2 ========');
function kalikan(nilai1 = 0, nilai2 = 0) {
    return nilai1 * nilai2;
}
var num1 = 10.5, num2 = 2;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


// Jawaban Soal No. 3
console.log('\n\n======== Jawaban Soal No. 3 ========');
function introduce(name = 'Kosong', age = 0, address = 'Kosong', hobby = 'Kosong') {
    return 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!';
}
var name = "Yoyok Gunanto", age = 25, address = "Sleman, Yogyakarta", hobby = "Programming";
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);