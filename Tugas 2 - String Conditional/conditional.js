// Jawaban Soal 1 : If Else
var nama = 'Yoyok';
var peran = '';

if (nama == '') {
    console.log('Nama harus diisi!');
} else {
    if (peran == '') {
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
    } else if (peran == 'Penyihir') {
        console.log('Selamat datang di Dunia Werewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
    } else if (peran == 'Guard') {
        console.log('Selamat datang di Dunia Werewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    } else if (peran == 'Werewolf') {
        console.log('Selamat datang di Dunia Werewolf, ' + nama);
        console.log('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    } else {
        console.log('Peran yang anda massukkan salah. Pilih pran berikut ini untuk memulai : Penyihir, Guard, Werewolf)');
    }
}

console.log('==========================================\n');

//Jawaban Soal 2 : Switch Case
var hari = 31;
var bulan = 12;
var tahun = 2010;
var bulan_nama;

if (hari < 1 || hari > 31) {
    console.log('Tanggal salah!');
} else {
    if (tahun < 1900 || tahun > 2200 || tahun.length < 4) {
        console.log('Tahun salah!');
    } else {
        if (bulan < 1 || bulan > 12) {
            console.log('Bulan salah!');
        } else {
            switch (bulan) {
                case 1: {
                    bulan_nama = 'Januari';
                    break;
                }
                case 2: {
                    bulan_nama = 'Februari';
                    break;
                }
                case 3: {
                    bulan_nama = 'Maret';
                    break;
                }
                case 4: {
                    bulan_nama = 'April';
                    break;
                }
                case 5: {
                    bulan_nama = 'Mei';
                    break;
                }
                case 6: {
                    bulan_nama = 'Juni';
                    break;
                }
                case 7: {
                    bulan_nama = 'Juli';
                    break;
                }
                case 8: {
                    bulan_nama = 'Agustus';
                    break;
                }
                case 9: {
                    bulan_nama = 'September';
                    break;
                }
                case 10: {
                    bulan_nama = 'Oktobet';
                    break;
                }
                case 11: {
                    bulan_nama = 'November';
                    break;
                }
                case 12: {
                    bulan_nama = 'Desember';
                    break;
                }
            }
            console.log(hari + ' ' + bulan_nama + ' ' + tahun);
        }
    }
}

console.log('==========================================\n');