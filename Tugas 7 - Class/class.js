// Jawaban No 1. Animal Class
// Release 0
console.log('========= Jawaban 1 Animal Class ==========');
class Animal {
    constructor(brand, legs = 4, cold_blooded = false) {
        this._carname = brand;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
    get name() {
        return this._carname;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

// Release 1
class Ape extends Animal {
    constructor(brand) {
        super(brand, 2);
    }
    yell() {
        console.log(this._carname + ' memiliki kaki ' + this.legs + ' dan bersuara Auooooooo');
    }
}
class Frog extends Animal {
    constructor(brand) {
        super(brand, 2);
    }
    jump() {
        console.log(this._carname + ' memiliki kaki ' + this.legs + ' dan bersuara hop hop');
    }
}
var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
var kodok = new Frog("buduk");
kodok.jump(); // "hop hop" 



// Jawaban No 2. Function to Class
console.log('\n\n========= Jawaban 2 Function to Class ==========');
class Clock {
    // Code di sini
    constructor(brand) {
        this.template = brand.template;
        this.timer;
    }
    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        var output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    };
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    };
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();  