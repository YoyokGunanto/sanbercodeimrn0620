import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
// import Icon2 from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";

export default class AboutScreen extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={styles.titlePage}>Tentang Saya</Text>
                    {/* <Icon style={styles.profil} name="account-circle" /> */}
                    <View style={{ alignItems: "center", padding: 10 }}>
                        <Image source={require('../Tugas13/images/me.jpg')} style={{ width: 150, height: 150, borderRadius: 75 }} />
                    </View>
                    <Text style={styles.nameUser}>Yoyok Gunanto</Text>
                    <Text style={styles.userTitle}>React Native Developer</Text>
                    <View style={styles.boxSection}>
                        <Text style={{ color: '#003366' }}>Portofolio</Text>
                        <View style={styles.lineStyle} />
                        <View style={styles.boxSkil}>
                            <TouchableOpacity>
                                <View style={{ alignItems: 'center', }}>
                                    <Icon2 style={styles.portoIcon} name="gitlab" />
                                    <Text style={styles.acsounSkil}>@YoyokGunanto</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{ alignItems: 'center', }}>
                                    <Icon2 style={styles.portoIcon} name="github-circle" />
                                    <Text style={styles.acsounSkil}>@YoyokGunanto</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.boxSection}>
                        <Text style={{ color: '#003366' }}>Hubungi Saya</Text>
                        <View style={styles.lineStyle} />
                        <View style={styles.boxSocial}>
                            <TouchableOpacity>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5, alignItems: 'center' }}>
                                    <Icon2 style={styles.socialIcon} name="facebook-box" />
                                    <Text style={{ marginLeft: 20, color: '#003366', fontSize: 15, fontWeight: 'bold', }}>Yoyok Gunanto</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5, alignItems: 'center' }}>
                                    <Icon2 style={styles.socialIcon} name="instagram" />
                                    <Text style={{ marginLeft: 20, color: '#003366', fontSize: 15, fontWeight: 'bold', }}>@yyk_gnto</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5, alignItems: 'center' }}>
                                    <Icon2 style={styles.socialIcon} name="twitter" />
                                    <Text style={{ marginLeft: 20, color: '#003366', fontSize: 15, fontWeight: 'bold', }}>@YGnto</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text></Text>
                    <Text></Text>
                </ScrollView>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    titlePage: {
        fontSize: 27,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#003366',
        paddingTop: 20,
    },
    profil: {
        textAlign: 'center',
        fontSize: 150,
        color: 'grey'
    },
    nameUser: {
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#003366',
        paddingTop: 10,
    },
    userTitle: {
        fontSize: 15,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#3EC6FF',
        paddingTop: 5,
    },
    boxSection: {
        backgroundColor: '#EFEFEF',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 10,
        padding: 5,
        justifyContent: 'center'
    },
    lineStyle: {
        borderWidth: 1,
        borderColor: '#003366',
        marginTop: 5,
    },
    boxSkil: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 15,
        paddingBottom: 15
    },
    portoIcon: {
        fontSize: 50,
        color: '#3EC6FF'
    },
    acsounSkil: {
        color: '#003366',
        paddingTop: 10,
        fontWeight: 'bold',
        fontSize: 15,
    },
    boxSocial: {
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 15,
    },
    socialIcon: {
        marginLeft: 70,
        fontSize: 50,
        color: '#3EC6FF'
    }
})
