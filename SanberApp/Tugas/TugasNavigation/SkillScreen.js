import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, Image, FlatList } from 'react-native';

import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import SkillDetail from "../Tugas14/components/skillDetail";
import data from '../Tugas14/skillData.json';

export default class SkillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ elevation: 2, borderStyle: 'solid', marginBottom: 5 }}>
                    {/* <Image source={require('../Tugas13/images/logo.png')} style={styles.logo} /> */}
                    <View style={styles.boxAcount}>
                        <IconMCI style={styles.iconUser} name='account-circle' />
                        <View style={styles.boxName}>
                            <Text>Hai,</Text>
                            <Text style={styles.name}>Yoyok Gunanto</Text>
                        </View>
                    </View>
                    <Text style={styles.textSkill}>SKILL</Text>
                    <View style={styles.lineStyle} />
                    <View style={styles.boxSkill}>
                        <TouchableOpacity style={styles.ButtonSkill}>
                            <Text style={styles.ButtonSkillText}>Library  Framework</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ButtonSkill}>
                            <Text style={styles.ButtonSkillText}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ButtonSkill}>
                            <Text style={styles.ButtonSkillText}>Teknologi</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <ScrollView> */}
                {/* <SkillDetail /> */}
                <FlatList
                    data={data.items}
                    renderItem={(skill) => <SkillDetail skill={skill.item} />}
                    keyExtractor={(item) => item.id.toString()}
                // ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
                />
                {/* </ScrollView> */}
                <StatusBar style="auto" />
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    logo: {
        // position: absolute,
        width: 187.5,
        height: 51,
        left: 170,
        top: 10,
    },
    boxAcount: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconUser: {
        color: '#3EC6FF',
        fontSize: 40,
        marginLeft: 20
    },
    boxName: {
        flexDirection: 'column',
        marginLeft: 10,
    },
    name: {
        fontSize: 16,
        color: '#003366',
    },
    textSkill: {
        color: '#003366',
        fontSize: 35,
        marginLeft: 15,
        marginTop: 5,
    },
    lineStyle: {
        borderWidth: 2,
        width: 330,
        borderColor: '#B4E9FF',
        marginTop: 1,
        marginLeft: 15,
    },
    boxSkill: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
    },
    ButtonSkill: {
        height: 32,
        marginTop: 10,
        backgroundColor: '#B4E9FF',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        paddingLeft: 10,
        paddingRight: 10,
    },
    ButtonSkillText: {
        color: '#003366',
        fontSize: 12,
        fontWeight: 'bold'
    }
});

