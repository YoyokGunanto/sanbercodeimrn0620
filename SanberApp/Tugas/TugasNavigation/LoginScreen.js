import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, Button } from 'react-native';
import * as Linking from 'expo-linking';

export default class LoginScreen extends React.Component {
    // _handlePress = () => {
    //     // Linking.openURL('https://expo.io');
    //     Linking.openURL(this.props.href);
    //     this.props.onPress && this.props.onPress();
    // };

    render() {
        return (
            <View style={styles.container}>
                {/* <Text {...this.props} onPress={this._handlePress}>
                    {this.props.children}LINK
                </Text> */}
                <Image source={require('../Tugas13/images/logo.png')} style={styles.logo} />
                <Text style={styles.titlePage}>Login</Text>
                <View style={styles.boxForm}>
                    <View style={{ alignItems: 'stretch' }}>
                        <Text style={styles.textField}>Username / Email</Text>
                        <TextInput style={styles.inputText}
                            placeholder=" Username / Email"
                        />
                    </View>
                    <View style={{ alignItems: 'stretch' }}>
                        <Text style={styles.textField}>Password</Text>
                        <TextInput style={styles.inputText}
                            secureTextEntry
                            autoCompleteType="password"
                            placeholder=" Password"
                        />
                    </View>
                </View>
                <View style={styles.boxButton}>
                    <TouchableOpacity style={styles.ButtonLogin}>
                        <Text style={styles.ButtonText}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.FormText}>atau</Text>
                    <TouchableOpacity style={styles.ButtonDaftar}>
                        <Text style={styles.ButtonText}>Daftar</Text>
                    </TouchableOpacity>
                </View>
                {/* <StatusBar style="auto" /> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    logo: {
        marginTop: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titlePage: {
        fontSize: 25,
        marginTop: 40,
        // fontWeight: 'bold',
        textAlign: 'center',
        color: '#003366',
    },
    boxForm: {
        alignItems: 'center',
        marginTop: 20,
    },
    textField: {
        fontSize: 15,
        color: '#003366',
        marginTop: 10,
        textAlign: 'left',
    },
    inputText: {
        width: 280,
        height: 38,
        borderColor: '#003366',
        borderWidth: 1,
        borderRadius: 4,
        padding: 5,
    },
    boxButton: {
        alignItems: 'center',
        paddingTop: 25,
    },
    ButtonLogin: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20
    },
    FormText: {
        fontSize: 20,
        marginTop: 10,
        color: '#3EC6FF',
    },
    ButtonDaftar: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#003366',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20
    },
    ButtonText: {
        color: 'white',
        fontSize: 20
    }

})
