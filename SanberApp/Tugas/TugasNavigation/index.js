import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { SignIn, Skill, Home, Project, Add, Details } from './Screen';

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SkillleStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();


const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="About" component={Home} />
        </HomeStack.Navigator>
    );
}

const ProjectStackScreen = () => {
    return (
        <ProjectStack.Navigator>
            <ProjectStack.Screen name="Project" component={Project} />
            <ProjectStack.Screen name="Details" component={Details} options={({ route }) => ({
                title: route.params.name
            })} />
        </ProjectStack.Navigator>
    );
}

const AddStackScreen = () => {
    return (
        <AddStack.Navigator>
            <AddStack.Screen name="Add" component={Add} />
            <AddStack.Screen name="Details" component={Details} options={({ route }) => ({
                title: route.params.name
            })} />
        </AddStack.Navigator>
    );
}

const SkillStackScreen = () => {
    return (
        <SkillleStack.Navigator>
            <SkillleStack.Screen name="Skill" component={Skill} />
        </SkillleStack.Navigator>
    );
}

const TabsScreen = () => {
    return (
        <Tabs.Navigator>
            <Tabs.Screen name="About" component={HomeStackScreen} />
            <Tabs.Screen name="Skill" component={SkillStackScreen} />
            <Tabs.Screen name="Project" component={ProjectStackScreen} />
            <Tabs.Screen name="Add" component={AddStackScreen} />
        </Tabs.Navigator>
    );
}

const Drawer = createDrawerNavigator();


export default function App() {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name='Login' component={SignIn} />
                <Drawer.Screen name='About' component={TabsScreen} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}