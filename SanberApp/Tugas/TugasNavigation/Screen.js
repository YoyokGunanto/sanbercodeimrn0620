import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import SkillScreen from "./SkillScreen";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 5
    }
});

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);

export const Home = ({ navigation }) => (
    <AboutScreen />
);


export const Profile = ({ navigation }) => {
    return (
        <LoginScreen />
    );
};

export const Skill = ({ navigation }) => {
    return (
        <SkillScreen />
    );
};


export const SignIn = ({ navigation }) => {
    return (
        <LoginScreen />
    );
};

export const Project = ({ navigation }) => {
    return (
        <ScreenContainer>
            <Text>Halaman Proyek</Text>
            <Button title="Detail Proyek" onPress={() => navigation.push('Details', { name: 'Detail Proyek' })} />
        </ScreenContainer>
    );
};

export const Add = ({ navigation }) => {
    return (
        <ScreenContainer>
            <Text>Halaman Tambah</Text>
            <Button title="Tambah" onPress={() => navigation.push('Details', { name: 'Tambah' })} />
        </ScreenContainer>
    );
};

export const Details = ({ route }) => (
    <ScreenContainer>
        <Text>Details Screen</Text>
        {route.params.name && <Text>{route.params.name}</Text>}
    </ScreenContainer>
);
