import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillDetail extends React.Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <View style={styles.boxSkilItem}>
                        <IconMCI style={styles.icon} name={skill.iconName} />
                        <View style={styles.boxText}>
                            <Text style={styles.textNameSkill}>{skill.skillName}</Text>
                            <Text style={styles.textKategoriSkill}>{skill.categoryName}</Text>
                            <Text style={styles.textPerceniSkill}>{skill.percentageProgress}</Text>
                        </View>
                        <IconMCI style={styles.iconChevronRight} name="chevron-right" />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        padding: 5
    },
    boxSkilItem: {
        flexDirection: 'row',
        backgroundColor: '#B4E9FF',
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5,
        elevation: 3,
        borderStyle: 'solid',
        alignItems: 'center'
    },
    icon: {
        fontSize: 70,
        color: '#003366',
    },
    boxText: {
        width: 160,
        marginLeft: 10,
    },
    textNameSkill: {
        fontSize: 23,
        fontWeight: 'bold',
        color: '#003366'
    },
    textKategoriSkill: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF'
    },
    textPerceniSkill: {
        fontSize: 50,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'right'
    },
    iconChevronRight: {
        fontSize: 100,
        color: '#003366',
    },
});