import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native';

export default class Registration extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Image source={require('./images/logo.png')} style={styles.logo} />
                    <Text style={styles.titlePage}>Register</Text>
                    <View style={styles.boxForm}>
                        <View style={{ alignItems: 'stretch' }}>
                            <Text style={styles.textField}>Username</Text>
                            <TextInput style={styles.inputText}
                                placeholder=" Username"
                            />
                        </View>
                        <View style={{ alignItems: 'stretch' }}>
                            <Text style={styles.textField}>Email</Text>
                            <TextInput style={styles.inputText}
                                autoCompleteType="email"
                                keyboardType="email-address"
                                textContentType="emailAddress"
                                placeholder=" Email"
                            />
                        </View>
                        <View style={{ alignItems: 'stretch' }}>
                            <Text style={styles.textField}>Password</Text>
                            <TextInput style={styles.inputText}
                                secureTextEntry
                                autoCompleteType="password"
                                placeholder=" Password"
                            />
                        </View>
                        <View style={{ alignItems: 'stretch' }}>
                            <Text style={styles.textField}>Ulangi Password</Text>
                            <TextInput style={styles.inputText}
                                secureTextEntry
                                autoCompleteType="password"
                                placeholder=" Ulangi Password"
                            />
                        </View>
                    </View>
                    <View style={styles.boxButton}>
                        <TouchableOpacity style={styles.ButtonDaftar}>
                            <Text style={styles.ButtonText}>Daftar</Text>
                        </TouchableOpacity>
                        <Text style={styles.FormText}>atau</Text>
                        <TouchableOpacity style={styles.ButtonLogin}>
                            <Text style={styles.ButtonText}>Masuk</Text>
                        </TouchableOpacity>
                    </View>
                    <Text></Text>
                    <Text></Text>
                </ScrollView>
                {/* <StatusBar style="auto" /> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titlePage: {
        fontSize: 25,
        marginTop: 40,
        // fontWeight: 'bold',
        textAlign: 'center',
        color: '#003366',
    },
    boxForm: {
        alignItems: 'center',
        marginTop: 20,
    },
    textField: {
        fontSize: 15,
        color: '#003366',
        marginTop: 10,
        textAlign: 'left',
    },
    inputText: {
        width: 280,
        height: 38,
        borderColor: '#003366',
        borderWidth: 1,
        borderRadius: 4,
        padding: 5,
    },
    boxButton: {
        alignItems: 'center',
        paddingTop: 25,
    },
    ButtonLogin: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20
    },
    FormText: {
        fontSize: 20,
        marginTop: 10,
        color: '#3EC6FF',
    },
    ButtonDaftar: {
        height: 40,
        width: 150,
        marginTop: 10,
        backgroundColor: '#003366',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20
    },
    ButtonText: {
        color: 'white',
        fontSize: 20
    }

})
