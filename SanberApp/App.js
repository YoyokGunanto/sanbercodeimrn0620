import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import Registration from './Tugas/Tugas13/Registration';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import Main from './Tugas/Tugas14/components/Main';
import SkillScreen from './Tugas/Tugas14/SkillScreen';
import Navigations from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    // Tugas 11
    // <View style={styles.container}>
    //   <Text>Hallo Everyone!</Text>
    //   <Text>I'am, Yoyok Gunanto ready to learn Bootcamp React Native</Text>
    //   <Text>with SanberCode</Text>
    //   <Text>Keep the spirit and keep learning.</Text>
    // <StatusBar style="auto" />
    // </View>

    // Tugas 12
    // <YoutubeUI />

    // Tugas 13
    // <Registration />
    // <LoginScreen />
    // <AboutScreen />

    // Tugas 14
    // <Main />
    // <SkillScreen />

    // Tugas 15
    // <Navigations />
    // <TugasNavigation />

    // Quiz 3
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
