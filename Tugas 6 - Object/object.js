// Jawaban No 1
console.log('========= No 1 - (Array to Object) =========');
function arrayToObject(arr) {
    // Code di sini 
    var hasil = {};
    var umur;
    var now = new Date();
    var thisYear = now.getFullYear();
    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][3] == undefined || arr[i][3] > thisYear) {
                umur = 'Invalid birth year';
            } else {
                umur = thisYear - arr[i][3];
            }
            hasil[(i + 1) + '. ' + arr[i][0] + ' ' + arr[i][1]] = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: umur,
            }
        }
    } else {
        hasil = '""';
    }
    console.log(hasil);
}
// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
arrayToObject(people2);
/*
    1. Tony Stark: {
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: {
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]); // ""


// Jawaban No 2
console.log('\n\n========= No 2 - (Shopping Time) =========');
function shoppingTime(memberId = '', money = 0) {
    // you can only write your code here!
    var hasil;
    if (memberId == '') {
        hasil = 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money <= 50000) {
        hasil = 'Mohon maaf, uang tidak cukup';
    } else {
        var barang = [
            ['Sepatu Stacattu', 1500000],
            ['Baju Zoro', 500000],
            ['Baju H&N', 250000],
            ['Sweater Uniklooh', 175000],
            ['Casing Handphone', 50000],
        ];
        var sisa_money = money;
        var list = [];
        for (var i = 0; i < barang.length; i++) {
            if (sisa_money >= barang[i][1]) {
                list.push(barang[i][0]);
                sisa_money -= barang[i][1];
            }
        }
        hasil = {
            memberId: memberId,
            money: money,
            listPurchased: list,
            changeMoney: sisa_money,
        }
    }
    return hasil;
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Jawaban No 3
console.log('\n\n========= No 3 - (Naik Angkot) =========');
function naikAngkot(arrPenumpang = '') {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var hasil = [];
    if (arrPenumpang != '') {
        for (var i = 0; i < arrPenumpang.length; i++) {
            var rute_awal = 0;
            for (var x = 0; x < rute.length; x++) {
                if (arrPenumpang[i][1] === rute[x]) {
                    rute_awal = x;
                    break;
                }
            }
            var rute_akhir = 0;
            for (var j = 0; j < rute.length; j++) {
                if (arrPenumpang[i][2] === rute[j]) {
                    rute_akhir = j;
                    break;
                }
            }
            var bayar = 0;
            for (var y = rute_awal; y < rute_akhir; y++) {
                bayar += 2000;
            }
            hasil[i] = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: bayar,
            }
        }
    }
    return hasil;
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]