console.log('\n====== Jawaban Nomor 1 (Looping While) ========');
console.log('\nLOOPING PERTAMA');
var nilai1 = 2;
while (nilai1 <= 20) {
    console.log(nilai1 + ' - I love coding');
    nilai1 += 2;
}

console.log('\nLOOPING KEDUA');
var nilai2 = 20;
while (nilai2 >= 2) {
    console.log(nilai2 + ' - I will become a mobile developer');
    nilai2 -= 2;
}

console.log('\n\n====== Jawaban Nomor 2 (Looping menggunakan for) ========\n');
for (nilai3 = 1; nilai3 <= 20; nilai3++) {
    if (nilai3 % 3 == 0) {
        if (nilai3 % 2 == 0) {
            console.log(nilai3 + ' - Berkualitas');
        } else {
            console.log(nilai3 + ' - I Love Coding');
        }
    } else {
        if (nilai3 % 2 == 0) {
            console.log(nilai3 + ' - Berkualitas');
        } else {
            console.log(nilai3 + ' - Santai');
        }
    }
}

console.log('\n\n====== Jawaban Nomor 3 (Membuat Persegi Panjang #) ========\n');
var loop1 = 0;
for (loop1; loop1 < 4; loop1++) {
    var loop2 = 0;
    var nilai4 = '';
    while (loop2 < 8) {
        nilai4 += '#';
        loop2++;
    }
    console.log(nilai4);
}

console.log('\n\n====== Jawaban Nomor 4 (Membuat Tangga) ========\n');
var ulang1 = 0;
var ulang2 = 0;
var nilai5 = '';
while (ulang2 < 7) {
    nilai5 += '#';
    console.log(nilai5);
    ulang2++;
}

console.log('\n\n====== Jawaban Nomor 5 (Membuat Papan Catur) ========\n');
var catur1 = 0;
var nilai6 = 0;
for (catur1; catur1 < 8; catur1++) {
    var catur2 = 0;
    var papan = '';
    for (catur2; catur2 < 8; catur2++) {
        if (nilai6 % 2 == 1) {
            papan += '#';
        } else {
            papan += ' ';
        }
        nilai6++;
    }
    console.log(papan);
    nilai6++;
}