// Jawaban No 1 Mengubah fungsi menjadi fungsi arrow
console.log('=========== No 1 ===========');
// ES5 
// const golden = function goldenFunction() {
//     console.log("this is golden!!")
// }
// golden();

//ES6
const golden = () => console.log("this is golden!!");
golden();



// Jawaban No 2 Sederhanakan menjadi Object literal di ES6
console.log('\n\n=========== No 2 ===========');
// ES5
// const newFunction = function literal(firstName, lastName) {
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function () {
//             console.log(firstName + " " + lastName)
//             return
//         }
//     }
// }
// newFunction("William", "Imoh").fullName()

// ES6
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}
newFunction("William", "Imoh").fullName();



// Jawaban No 3 Destructuring
console.log('\n\n=========== No 3 ===========');
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};
// ES5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ES 6
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);



// Jawaban No 4 Array Spreading
console.log('\n\n=========== No 4 ===========');
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// ES5
// const combined = west.concat(east)

// ES6
const combined = [...west, ...east];
console.log(combined);



// Jawaban No 5 Template Literals
console.log('\n\n=========== No 5 ===========');
const planet = "earth";
const view = "glass";
// ES5
// var before = 'Lorem ' + view + ' dolor sit amet, ' +
//     'consectetur adipiscing elit, ' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'

// ES6
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam`;
console.log(before);