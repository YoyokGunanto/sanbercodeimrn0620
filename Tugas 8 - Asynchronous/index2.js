var readBooksPromise = require('./promise.js');

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
];

// Lanjutkan code untuk menjalankan function readBooksPromise 
console.log('========== PROMISE ==========');
var timeStart = 10000;
readBooksPromise(timeStart, books[0])
    .then(
        response => readBooksPromise(response, books[1])
    )
    .then(
        response => readBooksPromise(response, books[2])
    )
    .catch(
        response => console.log(response)
    );