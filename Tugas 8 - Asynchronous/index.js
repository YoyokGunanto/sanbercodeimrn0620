// di index.js
var readBooks = require('./callback.js');

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
];

// Tulis code untuk memanggil function readBooks di sini
console.log('========== CALLBACK ==========');
var timeStart = 10000;
readBooks(timeStart, books[0], function (response) {
    readBooks(response, books[1], function (response) {
        readBooks(response, books[2], function (response) {
            console.log('Selesai membaca! Sisa waktu : ' + response);
        });
    });
});
